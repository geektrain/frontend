import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

import AdminDashboard from './pages/admin-dashboard.page.vue';
import Article from './pages/article.page.vue';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: '/',
    component: AdminDashboard
  },
  {
    path: '/article/:articleId',
    component: Article
  }
];

const router: VueRouter = new VueRouter({
  mode: 'history',
  routes
});

export default router;
