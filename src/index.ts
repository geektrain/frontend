import Vue from 'vue';
import VueFilterDateFormat from 'vue-filter-date-format';
import router from './router';
import App from './app.vue';

Vue.use(VueFilterDateFormat, {
  monthNames: [
    'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
    'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
  ],
  monthNamesShort: [
    'Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
    'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'
  ]
});

// tslint:disable-next-line
new Vue({
  el: '#app',
  router,
  render: h => h(App)
});
