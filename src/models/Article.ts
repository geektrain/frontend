export default class Article {
  constructor(
    public id: string = '',
    public title: string = '',
    public content: string = '',
    public lastUpdate?: Date
  ) {}

  public static build(dto: any = {}): Article {
    return new Article(
      dto.id || '',
      dto.title || '',
      dto.content || '',
      dto.lastUpdate
    );
  }
}
