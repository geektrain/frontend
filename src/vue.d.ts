declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module 'vue-filter-date-format';

declare module 'jodit-vue';

interface IArticle {
  id: string;
  title: string;
  createdAt: Date;
}


