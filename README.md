# GeekTrains frontend

## Installation

Clone the repo and run `npm install`

## How to run

Run `npm start`. App will be accessible via `http://localhost:3000`